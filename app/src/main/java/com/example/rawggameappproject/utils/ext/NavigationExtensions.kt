package com.example.rawggameappproject.utils.ext

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import com.example.rawggameappproject.R
import project.rawg.utils.extensions.whenStateAtLeast

private const val PREVIOUS_FRAGMENT_TAG_ARG = "PREVIOUS_FRAGMENT_TAG_ARG"
private var backPressedTime = 0L
private const val MIN_BACK_PRESSED_TIME = 2000
private const val MIN_BACK_STACK_ENTRY_COUNT = 2

fun Fragment.popScreen() {
    requireActivity().hideKeyboard()

    val fragmentManager = activity?.supportFragmentManager ?: childFragmentManager
    whenStateAtLeast(Lifecycle.State.STARTED) {
        fragmentManager.popBackStack()
    }
}

fun FragmentActivity.popFeature() {
    if (supportFragmentManager.backStackEntryCount < MIN_BACK_STACK_ENTRY_COUNT) {
        val backText = resources.getString(R.string.back_text)
        val currentTime = System.currentTimeMillis()
        if (currentTime - backPressedTime < MIN_BACK_PRESSED_TIME) {
            finish()
        } else {
            backPressedTime = currentTime
            this.createSnackBar(findViewById(R.id.container), backText)
        }
    } else {
        whenStateAtLeast(Lifecycle.State.STARTED) {
            supportFragmentManager.popBackStack()
        }
    }
}

private fun Fragment.getPreviousTag(): String? = arguments?.getString(PREVIOUS_FRAGMENT_TAG_ARG)

fun Fragment.getCurrentScreen(): Fragment? =
    childFragmentManager.findFragmentById(R.id.container)

fun Fragment.replaceScreen(
    fragment: Fragment,
    popCurrent: Boolean = false,
    clearBackStack: Boolean = false,
    addToBackStack: Boolean = true,
    requestCode: Int? = null,
    tag: String = fragment::class.java.name,

    fragmentManager: FragmentManager = activity?.supportFragmentManager ?: childFragmentManager
) = whenStateAtLeast(Lifecycle.State.STARTED) {
    requireActivity().hideKeyboard()
    if (clearBackStack)
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    fragmentManager.commit {
        if (popCurrent) {
            this@replaceScreen.popScreen()
            getCurrentScreen()
                ?.let { it.getPreviousTag() ?: it::class.java.name }
                ?.let { fragment.appendArgs(PREVIOUS_FRAGMENT_TAG_ARG to it) }
        }
        replace(R.id.container, fragment, tag)
        if (addToBackStack) addToBackStack(tag)
        if (requestCode != null) fragment.setTargetFragment(this@replaceScreen, requestCode)

    }
}

fun Fragment.addScreen(
    fragment: Fragment,
    popCurrent: Boolean = false,
    addToBackStack: Boolean = true,
    requestCode: Int? = null,
    tag: String = fragment::class.java.name,
    fragmentManager: FragmentManager = activity?.supportFragmentManager ?: childFragmentManager
) = whenStateAtLeast(Lifecycle.State.STARTED) {
    requireActivity().hideKeyboard()
    fragmentManager.commit {
        if (popCurrent) {
            getCurrentScreen()
                ?.let { it.getPreviousTag() ?: it::class.java.name }
                ?.let {
                    fragment.appendArgs(PREVIOUS_FRAGMENT_TAG_ARG to it)
                    fragmentManager.popBackStack()
                }
        }
        add(R.id.container, fragment, tag)
        if (addToBackStack) addToBackStack(tag)
        if (requestCode != null) fragment.setTargetFragment(this@addScreen, requestCode)
    }
}

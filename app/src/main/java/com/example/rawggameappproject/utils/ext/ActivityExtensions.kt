package com.example.rawggameappproject.utils.ext

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.rawggameappproject.R
import com.google.android.material.behavior.SwipeDismissBehavior
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar


fun Activity.hideKeyboard() {
    currentFocus?.hideKeyboard()
}

fun Fragment.hideKeyboardDrop() {
    requireActivity().hideKeyboard()
}

fun View.hideKeyboard() {
    post {
        (context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(windowToken, 0)
    }
}

fun Fragment.createSnackBar(view: View, message: String) {
    val behavior = BaseTransientBottomBar.Behavior().apply {
        setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY)
    }
    context?.let {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .setActionTextColor(ContextCompat.getColor(it, R.color.grey))
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).setBehavior(behavior)
            .setBackgroundTint(ContextCompat.getColor(it, R.color.black))
            .show()
    }
}

fun FragmentActivity.createSnackBar(view: View, message: String) {
    val behavior = BaseTransientBottomBar.Behavior().apply {
        setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_ANY)
    }
    this.baseContext?.let {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .setActionTextColor(ContextCompat.getColor(it, R.color.grey))
            .setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE).setBehavior(behavior)
            .setBackgroundTint(ContextCompat.getColor(it, R.color.grey))
            .show()
    }
}

const val DEFAULT_THROTTLE_DURATION_IN_MILLIS = 300L
inline fun <V : View> V.onClick(
    throttleDuration: Long = DEFAULT_THROTTLE_DURATION_IN_MILLIS,
    crossinline listener: () -> Unit
): V {
    setOnClickListener(SafeClickListener(throttleDuration) { listener.invoke() })
    return this
}

class SafeClickListener(
    throttleDuration: Long,
    private val listener: View.OnClickListener,
) : View.OnClickListener {
    private val doubleClickPreventer = DoubleClickPreventer(throttleDuration)

    override fun onClick(v: View) {
        if (doubleClickPreventer.prevent()) {
            return
        }
        listener.onClick(v)
    }
}

private class DoubleClickPreventer(
    private val throttleDuration: Long
) {
    private var lastClickTime = 0L

    fun prevent(): Boolean {
        val currentTime = System.currentTimeMillis()
        val spentTime = currentTime - lastClickTime
        if (spentTime in 1 until throttleDuration) {
            return true
        }
        lastClickTime = currentTime
        return false
    }
}

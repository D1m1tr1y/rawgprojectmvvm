package com.example.rawggameappproject.utils

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
sealed class GenreTypes(
    val Genre : String
):Parcelable{
    object Action : GenreTypes("action")
    object Indie : GenreTypes("indie")
    object Adventure : GenreTypes("adventure")
    object Rpg : GenreTypes("role-playing-games-rpg")
    object Strategy : GenreTypes("strategy")
    object Shooter : GenreTypes("shooter")
    object Casual : GenreTypes("casual")
    object Simulation : GenreTypes("simulation")
    object Puzzle : GenreTypes("puzzle")
    object Arcade : GenreTypes("arcade")
    object Platformer : GenreTypes("platformer")
    object MassivelyMultiplayer : GenreTypes("massively-multiplayer")
    object Racing : GenreTypes("racing")
    object Sports : GenreTypes("sports")
    object Fighting : GenreTypes("fighting")
    object BoardGames : GenreTypes("board-games")
    object Educational : GenreTypes("educational")
    object Card : GenreTypes("card")
    object Family : GenreTypes("family")
}

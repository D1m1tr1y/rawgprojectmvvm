package com.example.rawggameappproject.details.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.RoundedCornersTransformation
import com.example.rawggameappproject.databinding.ItemDetailsBinding
import com.example.rawggameappproject.main_page.ui.model.ShortScreenshotUi
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

class DetailsViewHolder (
    private val binding: ItemDetailsBinding
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup
    ) : this(
        ItemDetailsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    fun onBind(imageValue : ShortScreenshotUi) {
        with(binding) {
            image.load(imageValue.image){
                transformations (RoundedCornersTransformation (25f))
            }
        }

    }
}

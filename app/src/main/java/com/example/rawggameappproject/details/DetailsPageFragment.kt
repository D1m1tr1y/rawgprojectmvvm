package com.example.rawggameappproject.details

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rawggameappproject.R
import com.example.rawggameappproject.common.BaseFragment
import com.example.rawggameappproject.databinding.FragmentDetailsBinding
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.example.rawggameappproject.utils.Arguments
import com.example.rawggameappproject.utils.viewBinding
import com.example.rawggameappproject.details.adapter.DetailsPageAdapter
import com.example.rawggameappproject.details.adapter.DetailsViewHolder
import com.example.rawggameappproject.utils.ext.args
import com.example.rawggameappproject.utils.ext.withArgs
import timber.log.Timber

class DetailsPageFragment : BaseFragment(R.layout.fragment_details) {

    private val binding: FragmentDetailsBinding by viewBinding()

    companion object {
        fun newInstance(game: GameUi) =
            DetailsPageFragment().withArgs(Arguments.GAME_DATA to game)
    }
    private val game: GameUi by args(Arguments.GAME_DATA)

    private val adapter by lazy {
        DetailsPageAdapter()
    }

    override fun bind() {
        with(binding){
            rating.rating = game.rating
            ratingValue.text= game.rating.toString()
            gameName.text= game.name
        }
        adapter.setData(game.shortScreenshots)
    }

    override fun initViews(view: View) {
        with(binding){
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            recyclerView.setHasFixedSize(true)
            adapter.onAttachedToRecyclerView(recyclerView)
        }


    }


}
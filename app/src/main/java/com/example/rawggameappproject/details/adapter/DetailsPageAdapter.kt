package com.example.rawggameappproject.details.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rawggameappproject.R
import com.example.rawggameappproject.main_page.ui.model.ShortScreenshotUi
import timber.log.Timber

class DetailsPageAdapter : RecyclerView.Adapter<DetailsViewHolder>() {
    private val images = mutableListOf<ShortScreenshotUi>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_details, parent, false)
        return DetailsViewHolder(parent)
    }

    override fun getItemCount() = images.size


    override fun onBindViewHolder(holder: DetailsViewHolder, position: Int) {
        val listItem = images[position]

        holder.onBind(listItem)
    }
    @SuppressLint("NotifyDataSetChanged")
    fun setData( imagesList : List<ShortScreenshotUi>) {
        images.clear()
        images.addAll(imagesList)
        notifyDataSetChanged()
    }
}

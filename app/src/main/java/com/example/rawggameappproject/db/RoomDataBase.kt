package com.example.rawggameappproject.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.rawggameappproject.main_page.db.dao.MaInPageDao
import com.example.rawggameappproject.main_page.db.model.GameEntity
import com.example.rawggameappproject.main_page.db.model.ParentPlatformEntity
import com.example.rawggameappproject.main_page.db.model.RatingEntity
import com.example.rawggameappproject.main_page.db.model.ShortScreenshotEntity
import com.example.rawggameappproject.main_page.db.model.GenreEntity

@Database(
    entities = [
        GameEntity::class,
        GenreEntity::class,
        ParentPlatformEntity::class,
        RatingEntity::class,
        ShortScreenshotEntity::class
               ],
    version = 1
)
abstract class RoomDataBase : RoomDatabase(){
    abstract val gameDao : MaInPageDao
}
package com.example.rawggameappproject.db

import com.example.rawggameappproject.main_page.di.MainPageModule
import com.example.rawggameappproject.main_page.repository.LocalRepository
import com.example.rawggameappproject.main_page.repository.RemoteRepository
import com.example.rawggameappproject.utils.GenreTypes
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import com.example.rawggameappproject.main_page.api.PagingState
import com.example.rawggameappproject.main_page.model.Game
import com.example.rawggameappproject.main_page.model.GameCategoryModel
import com.example.rawggameappproject.utils.ext.collectFLow
import javax.inject.Inject

class GamesMediator @Inject constructor(
    private val remoteRepository: RemoteRepository,
    private val localRepository: LocalRepository,
    private val genreType: GenreTypes,
    @MainPageModule.Companion.DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) {

    private val dataStateFlow = MutableStateFlow(convertToModel(PagingState.Initial))

    fun data(): Flow<GameCategoryModel> = dataStateFlow

    suspend fun init(scope: CoroutineScope) {
        val state: PagingState<List<Game>> = dataStateFlow.value.dataState
        if (state is PagingState.Initial) {
            try {
                val localData =
                    localRepository.getGamesData(genreType.Genre).collectFLow(
                        defaultDispatcher,
                        scope
                    ) {
                        it
                    }
                if (localData.isEmpty()) {
                    val data = remoteRepository.initialLoading(genreType.Genre)
                    dataStateFlow.emit(convertToModel(PagingState.Content(data)))
                    localRepository.upsertGamesData(data, genreType.Genre)
                } else {
                    dataStateFlow.emit(convertToModel(PagingState.Content(localData)))
                }

            } catch (e: Exception) {
                throw e
            }
        }
    }

    suspend fun tryToLoadMore(index: Int, scope: CoroutineScope) {
        val state = dataStateFlow.value.dataState
        if (state is PagingState.Content && index == state.data.size - 1) {
            loadMore(scope)
        }
    }


    private suspend fun loadMore(scope: CoroutineScope) {
        val currentData = (dataStateFlow.value.dataState as? PagingState.Content)?.data
            ?: (dataStateFlow.value.dataState as? PagingState.Persist)?.data
            ?: return
        dataStateFlow.emit(convertToModel(PagingState.Paging(currentData)))
        try {
            remoteRepository.updateParams(
                genre = genreType.Genre,
                alreadyLoadedCount = localRepository.getGamesData(genreType.Genre)
                    .collectFLow(
                        defaultDispatcher,
                        scope
                    ) {
                        it
                    }.size
            )
            val data = remoteRepository.loadMore()
            dataStateFlow.emit(convertToModel(PagingState.Content(currentData.plus(data))))
            localRepository.upsertGamesData(data, genreType.Genre)
        } catch (e: Exception) {
            val flow = localRepository.getGamesData(genreType.Genre)
            val localData = flow.collectFLow(defaultDispatcher, scope) {
                it
            }
            if (localData.isEmpty()) {
            } else {
                dataStateFlow.emit(convertToModel(PagingState.Persist(localData)))
            }
            throw e
        }
    }

    private fun convertToModel(state: PagingState<List<Game>>) = GameCategoryModel(
        genreType = genreType,
        dataState = state,
    )
}

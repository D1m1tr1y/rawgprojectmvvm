package com.example.rawggameappproject.main_page.model

data class ShortScreenshot(
    val id: Int,
    val image: String
)
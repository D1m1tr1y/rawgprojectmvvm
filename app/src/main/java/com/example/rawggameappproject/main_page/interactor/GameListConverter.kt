package com.example.rawggameappproject.main_page.interactor

import com.example.rawggameappproject.main_page.api.PagingState
import com.example.rawggameappproject.main_page.model.Game
import com.example.rawggameappproject.main_page.model.GameCategoryModel
import com.example.rawggameappproject.main_page.ui.model.GameListUi
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.example.rawggameappproject.main_page.ui.model.GenreUi
import com.example.rawggameappproject.main_page.ui.model.ParentPlatformUi
import com.example.rawggameappproject.main_page.ui.model.RatingUi
import com.example.rawggameappproject.main_page.ui.model.ShortScreenshotUi

object GameListConverter {

    fun toGameListUi(model: GameCategoryModel): GameListUi =
        when (model.dataState) {
            is PagingState.Content ->
                GameListUi(
                    genre = model.genreType,
                    results = model.dataState.data.toGameUiList()
                )

            is PagingState.Paging ->
                GameListUi(
                    genre = model.genreType,
                    results = model.dataState.availableContent.toGameUiList()
                )

            is PagingState.Persist ->
                GameListUi(
                    genre = model.genreType,
                    results = model.dataState.data.toGameUiList()
                )
            else -> {GameListUi(genre = model.genreType,
                results = emptyList()
            )}
        }

    private fun List<Game>.toGameUiList() = map {
        GameUi(
            id = it.id,
            slug = it.slug,
            name = it.name,
            released = it.released,
            backgroundImage = it.backgroundImage,
            rating = it.rating,
            ratings = it.ratings.map { rating ->
                RatingUi(
                    id = rating.id,
                    title = rating.title,
                    count = rating.count,
                    percent = rating.percent
                )
            },
            ratingsCount = it.ratingsCount,
            metacritic = it.metacritic,
            parentPlatforms = it.parentPlatforms.map {result ->
                ParentPlatformUi(
                    id = result.id,
                    name = result.name,
                    slug = result.slug
                )
            },
            genres = it.genres.map {result ->
                GenreUi(
                    id = result.id,
                    name = result.name,
                    slug = result.slug
                )
            },
            esrbRatingName = it.esrbRatingName,
            shortScreenshots = it.shortScreenshots.map {result ->
                ShortScreenshotUi(
                    id = result.id,
                    image = result.image,
                )
            },
            added = it.added,
            esrbRatingId = it.esrbRatingId,
            esrbRatingSlug = it.esrbRatingSlug
        )
    }
}
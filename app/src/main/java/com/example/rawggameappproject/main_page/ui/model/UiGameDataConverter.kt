package com.example.rawggameappproject.main_page.ui.model

import com.example.rawggameappproject.main_page.model.Game
import com.example.rawggameappproject.main_page.model.Genre
import com.example.rawggameappproject.main_page.model.ParentPlatform
import com.example.rawggameappproject.main_page.model.Rating
import com.example.rawggameappproject.main_page.model.ShortScreenshot

object UiGameDataConverter {
    fun fromNetwork(response :  List<Game>):List<GameUi> =
        response.map { result ->
            GameUi(
                id = result.id,
                slug = result.slug,
                name = result.name,
                released = result.released,
                backgroundImage = result.backgroundImage,
                ratings = fromNetworkRatings(result.ratings),
                rating = result.rating,
                ratingsCount = result.ratingsCount,
                added = result.added,
                metacritic = result.metacritic,
                parentPlatforms = fromNetworkParentPlatforms(result.parentPlatforms),
                genres = fromNetworkGenres(result.genres),
                esrbRatingId = result.esrbRatingId,
                esrbRatingName = result.esrbRatingName,
                esrbRatingSlug = result.esrbRatingSlug,
                shortScreenshots = fromNetworkShortScreenshots(result.shortScreenshots)
            )
        }
    private fun fromNetworkRatings(response: List<Rating>): List<RatingUi> =
        response.map{result ->
            RatingUi(
                id = result.id,
                title = result.title,
                count = result.count,
                percent = result.percent
            )
        }
    private fun fromNetworkParentPlatforms(response : List<ParentPlatform>) : List<ParentPlatformUi> =
        response.map { result ->
            ParentPlatformUi(
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }
    private fun fromNetworkGenres(response: List<Genre>) : List<GenreUi> =
        response.map {result ->
            GenreUi(
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }
    private fun fromNetworkShortScreenshots(response: List<ShortScreenshot>): List<ShortScreenshotUi> =
        response.map{result ->
            ShortScreenshotUi(
                id = result.id,
                image = result.image
            )
        }



}
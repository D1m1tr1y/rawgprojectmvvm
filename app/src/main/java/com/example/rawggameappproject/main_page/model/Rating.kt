package com.example.rawggameappproject.main_page.model

data class Rating(
    val id: Int,
    val title: String,
    val count: Int,
    val percent: Float
)

package com.example.rawggameappproject.main_page.api.model

import com.google.gson.annotations.SerializedName

data class ParentPlatformResponse(
    @SerializedName("platform")
    val parentPlatformInfo: ParentPlatformInfoResponse
)

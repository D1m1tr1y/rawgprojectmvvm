package com.example.rawggameappproject.main_page.repository

import com.example.rawggameappproject.main_page.model.Game

interface RemoteRepository {
   suspend fun getGamesData(genre : String,page : Int): List<Game>
   suspend fun initialLoading(genre: String): List<Game>
   suspend fun loadMore(): List<Game>
   fun updateParams(genre: String, alreadyLoadedCount: Int)

}
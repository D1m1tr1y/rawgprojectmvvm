package com.example.rawggameappproject.main_page.model

data class Game(
    val id: Int,
    val slug: String,
    val name: String,
    val released: String,
    val backgroundImage: String,
    val rating: Float,
    val ratings: List<Rating>,
    val ratingsCount: Int,
    val added: Int,
    val metacritic: Int?,
    val parentPlatforms: List<ParentPlatform>,
    val genres: List<Genre>,
    val esrbRatingId: Int?,
    val esrbRatingName: String?,
    val esrbRatingSlug: String?,
    val shortScreenshots: List<ShortScreenshot>
)

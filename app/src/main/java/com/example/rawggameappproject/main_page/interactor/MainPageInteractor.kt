package com.example.rawggameappproject.main_page.interactor

import com.example.rawggameappproject.db.GamesMediator
import com.example.rawggameappproject.main_page.ui.model.GameListUi
import com.example.rawggameappproject.utils.GenreTypes
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import javax.inject.Inject
import javax.inject.Named

class MainPageInteractor @Inject constructor(
    @Named("action") private val actionMediator: GamesMediator,
    @Named("indie") private val indieMediator: GamesMediator,
    @Named("adventure") private val adventureMediator: GamesMediator,
    @Named("rpg") private val rpgMediator: GamesMediator,
    @Named("strategy") private val strategyMediator: GamesMediator,
    @Named("shooter") private val shooterMediator: GamesMediator,
    @Named("casual") private val casualMediator: GamesMediator,
    @Named("simulation") private val simulationMediator: GamesMediator,
    @Named("puzzle") private val puzzleMediator: GamesMediator,
    @Named("arcade") private val arcadeMediator: GamesMediator,
    @Named("platformer") private val platformerMediator: GamesMediator,
    @Named("massive") private val massivelyMultiplayerMediator: GamesMediator,
    @Named("racing") private val racingMediator: GamesMediator,
    @Named("sports") private val sportsMediator: GamesMediator,
    @Named("fighting") private val fightingMediator: GamesMediator,
    @Named("family") private val familyMediator: GamesMediator,
    @Named("board") private val boardGamesMediator: GamesMediator,
    @Named("edu") private val educationalMediator: GamesMediator,
    @Named("card") private val cardMediator: GamesMediator
) : Interactor {
    override fun data(): Flow<List<GameListUi>> = combine(
        actionMediator.data(),
        indieMediator.data(),
        adventureMediator.data(),
        rpgMediator.data(),
        strategyMediator.data(),
        shooterMediator.data(),
        casualMediator.data(),
        simulationMediator.data(),
        puzzleMediator.data(),
        arcadeMediator.data(),
        platformerMediator.data(),
        massivelyMultiplayerMediator.data(),
        racingMediator.data(),
        sportsMediator.data(),
        fightingMediator.data(),
        familyMediator.data(),
        boardGamesMediator.data(),
        educationalMediator.data(),
        cardMediator.data()
    ) { values ->
        values.map { gameCategory ->
            GameListConverter.toGameListUi(gameCategory)
        }
    }

    override suspend fun initCategory(genre: GenreTypes, scope: CoroutineScope) {
        when (genre) {
            is GenreTypes.Action -> actionMediator.init(scope)
            is GenreTypes.Indie -> indieMediator.init(scope)
            is GenreTypes.Adventure -> adventureMediator.init(scope)
            is GenreTypes.Rpg -> rpgMediator.init(scope)
            is GenreTypes.Strategy -> strategyMediator.init(scope)
            is GenreTypes.Shooter -> shooterMediator.init(scope)
            is GenreTypes.Casual -> casualMediator.init(scope)
            is GenreTypes.Simulation -> simulationMediator.init(scope)
            is GenreTypes.Puzzle -> puzzleMediator.init(scope)
            is GenreTypes.Arcade -> arcadeMediator.init(scope)
            is GenreTypes.Platformer -> platformerMediator.init(scope)
            is GenreTypes.MassivelyMultiplayer -> massivelyMultiplayerMediator.init(scope)
            is GenreTypes.Racing -> racingMediator.init(scope)
            is GenreTypes.Sports -> sportsMediator.init(scope)
            is GenreTypes.Fighting -> fightingMediator.init(scope)
            is GenreTypes.Family -> familyMediator.init(scope)
            is GenreTypes.BoardGames -> boardGamesMediator.init(scope)
            is GenreTypes.Educational -> educationalMediator.init(scope)
            is GenreTypes.Card -> cardMediator.init(scope)
            else -> {}
        }
    }

    override suspend fun tryToLoadMore(genre: GenreTypes, index: Int, scope: CoroutineScope) {
        when (genre) {
            is GenreTypes.Action -> actionMediator.tryToLoadMore(index, scope)
            is GenreTypes.Indie -> indieMediator.tryToLoadMore(index, scope)
            is GenreTypes.Adventure -> adventureMediator.tryToLoadMore(index, scope)
            is GenreTypes.Rpg -> rpgMediator.tryToLoadMore(index, scope)
            is GenreTypes.Strategy -> strategyMediator.tryToLoadMore(index, scope)
            is GenreTypes.Shooter -> shooterMediator.tryToLoadMore(index, scope)
            is GenreTypes.Casual -> casualMediator.tryToLoadMore(index, scope)
            is GenreTypes.Simulation -> simulationMediator.tryToLoadMore(index, scope)
            is GenreTypes.Puzzle -> puzzleMediator.tryToLoadMore(index, scope)
            is GenreTypes.Arcade -> arcadeMediator.tryToLoadMore(index, scope)
            is GenreTypes.Platformer -> platformerMediator.tryToLoadMore(index, scope)
            is GenreTypes.MassivelyMultiplayer -> massivelyMultiplayerMediator.tryToLoadMore(index, scope)
            is GenreTypes.Racing -> racingMediator.tryToLoadMore(index, scope)
            is GenreTypes.Sports -> sportsMediator.tryToLoadMore(index, scope)
            is GenreTypes.Fighting -> fightingMediator.tryToLoadMore(index, scope)
            is GenreTypes.Family -> familyMediator.tryToLoadMore(index, scope)
            is GenreTypes.BoardGames -> boardGamesMediator.tryToLoadMore(index, scope)
            is GenreTypes.Educational -> educationalMediator.tryToLoadMore(index, scope)
            is GenreTypes.Card -> cardMediator.tryToLoadMore(index, scope)
            else -> {}
        }
    }

}

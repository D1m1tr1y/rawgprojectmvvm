package com.example.rawggameappproject.main_page.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.example.rawggameappproject.main_page.db.model.GameEntity
import com.example.rawggameappproject.main_page.db.model.ParentPlatformEntity
import com.example.rawggameappproject.main_page.db.model.RatingEntity
import com.example.rawggameappproject.main_page.db.model.ShortScreenshotEntity
import kotlinx.coroutines.flow.Flow
import com.example.rawggameappproject.main_page.db.model.GenreEntity

@Dao
interface MaInPageDao {

    @Query("SELECT * FROM games WHERE genre_type = :genre")
    fun getGames(genre: String): Flow<List<GameEntity>>

    @Upsert
    suspend fun upsertGames(genres: List<GameEntity>)

    @Query("SELECT * FROM genres WHERE game_id = :gameId")
    suspend fun getGenres(gameId: Long): List<GenreEntity>

    @Upsert
    suspend fun upsertGenres(games: List<GenreEntity>)

    @Query("SELECT * FROM parent_platforms WHERE game_id = :gameId")
    suspend fun getParentPlatforms(gameId: Long): List<ParentPlatformEntity>

    @Upsert
    suspend fun upsertParentPlatforms(games: List<ParentPlatformEntity>)

    @Query("SELECT * FROM ratings WHERE game_id = :gameId")
    suspend fun getRating(gameId: Long): List<RatingEntity>

    @Upsert
    suspend fun upsertRatings(games: List<RatingEntity>)

    @Query("SELECT * FROM short_screenshots WHERE game_id = :gameId")
    suspend fun getShortScreenshots(gameId: Long): List<ShortScreenshotEntity>

    @Upsert
    suspend fun upsertShortScreenshots(games: List<ShortScreenshotEntity>)
}

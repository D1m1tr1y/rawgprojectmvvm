package com.example.rawggameappproject.main_page.repository

import com.example.rawggameappproject.main_page.model.Game
import kotlinx.coroutines.flow.Flow

interface LocalRepository {
    suspend fun getGamesData(genre : String): Flow<List<Game>>
    suspend fun upsertGamesData(gamesData : List<Game>,genre: String)
}
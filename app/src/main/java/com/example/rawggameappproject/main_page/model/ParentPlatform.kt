package com.example.rawggameappproject.main_page.model

data class ParentPlatform(
    val id: Int,
    val name: String,
    val slug: String
)

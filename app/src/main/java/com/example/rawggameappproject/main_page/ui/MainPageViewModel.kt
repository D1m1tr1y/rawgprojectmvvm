package com.example.rawggameappproject.main_page.ui

import com.example.rawggameappproject.common.BaseViewModel
import com.example.rawggameappproject.main_page.interactor.Interactor
import com.example.rawggameappproject.main_page.ui.model.GameListUi
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.example.rawggameappproject.utils.GenreTypes
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.cancellation.CancellationException
@HiltViewModel
class MainPageViewModel @Inject constructor(
    private val interactor: Interactor
): BaseViewModel() {
    private val _gameData = MutableStateFlow<List<GameListUi>>(emptyList())
    val gameData = _gameData.asStateFlow()
    init {
        launch {
            try {
                interactor.data().collect { list ->
                    _gameData.tryEmit(list)
                }
            } catch (e: CancellationException) {
                Timber.e("/*/ Error ${e.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            }
        }
    }

    fun initCategory(genre: GenreTypes) {
        launch {
            try {
                interactor.initCategory(genre, this)
            } catch (e: CancellationException) {
                Timber.e("/*/ Error ${e.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            }
        }
    }

    fun readyToLoadMore(genre: GenreTypes, index: Int) {
        launch {
            try {
                interactor.tryToLoadMore(genre, index, this)
            } catch (e: CancellationException) {
                Timber.e("/*/ Error ${e.message}")
            } catch (t: Throwable) {
                Timber.e("/*/ Error ${t.message}")
            }
        }
    }

}

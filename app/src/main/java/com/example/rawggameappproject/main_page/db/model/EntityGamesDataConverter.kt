package com.example.rawggameappproject.main_page.db.model

import com.example.rawggameappproject.main_page.model.Game
import com.example.rawggameappproject.main_page.model.Genre
import com.example.rawggameappproject.main_page.model.ParentPlatform
import com.example.rawggameappproject.main_page.model.Rating
import com.example.rawggameappproject.main_page.model.ShortScreenshot

object EntityGamesDataConverter {

    fun toDataBase(response: List<Game>,genre:String): List<GameEntity> =
        response.map { result ->
            GameEntity(
                id = result.id,
                slug = result.slug,
                name = result.name,
                genreType = genre,
                released = result.released,
                backgroundImage = result.backgroundImage,
                rating = result.rating,
                ratingsCount = result.ratingsCount,
                added = result.added,
                metacritic = result.metacritic,
                esrbRatingId = result.esrbRatingId,
                esrbRatingName = result.esrbRatingName,
                esrbRatingSlug = result.esrbRatingSlug
            )
        }

    fun toDataBaseGenre(response: List<Genre>, gameId: Int): List<GenreEntity> =
        response.map { result ->
            GenreEntity(
                gameId = gameId,
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }

    fun toDataBaseRating(response: List<Rating>, gameId: Int): List<RatingEntity> =
        response.map { result ->
            RatingEntity(
                gameId = gameId,
                id = result.id,
                title = result.title,
                count = result.count,
                percent = result.percent
            )
        }

    fun toDataBaseParentPlatform(
        response: List<ParentPlatform>,
        gameId: Int
    ): List<ParentPlatformEntity> =
        response.map { result ->
            ParentPlatformEntity(
                gameId = gameId,
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }

    fun toDataBaseShortScreenshot(
        response: List<ShortScreenshot>,
        gameId: Int
    ): List<ShortScreenshotEntity> =
        response.map { result ->
            ShortScreenshotEntity(
                gameId = gameId,
                id = result.id,
                image = result.image
            )
        }

    fun fromDataBase(
        response: GameEntity,
        parentPlatform: List<ParentPlatformEntity>,
        rating: List<RatingEntity>,
        genre: List<GenreEntity>,
        shortScreenshot: List<ShortScreenshotEntity>

        ): Game =
            Game(
                id = response.id,
                slug = response.slug,
                name = response.name,
                released = response.released,
                backgroundImage = response.backgroundImage,
                rating = response.rating,
                ratingsCount = response.ratingsCount,
                added = response.added,
                metacritic = response.metacritic,
                esrbRatingId = response.esrbRatingId,
                esrbRatingName = response.esrbRatingName,
                esrbRatingSlug = response.esrbRatingSlug,
                parentPlatforms = fromDataBaseParentPlatforms(parentPlatform),
                ratings = fromDataBaseRating(rating),
                genres = fromDataBaseGenre(genre),
                shortScreenshots = fromDataBaseShortScreenshots(shortScreenshot)
            )


    private fun fromDataBaseParentPlatforms(response: List<ParentPlatformEntity>): List<ParentPlatform> =
        response.map { result ->
            ParentPlatform(
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }

    private fun fromDataBaseRating(response: List<RatingEntity>): List<Rating> =
        response.map { result ->
            Rating(
                id = result.id,
                title = result.title,
                count = result.count,
                percent = result.percent
            )
        }

    private fun fromDataBaseGenre(response: List<GenreEntity>): List<Genre> =
        response.map { result ->
            Genre(
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }
    private fun fromDataBaseShortScreenshots(response: List<ShortScreenshotEntity>): List<ShortScreenshot> =
        response.map { result->
            ShortScreenshot(
                id = result.id,
                image = result.image
            )
        }
}
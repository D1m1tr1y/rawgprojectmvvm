package com.example.rawggameappproject.main_page.ui.main_adapter

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import com.example.rawggameappproject.main_page.ui.model.GameListUi
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.example.rawggameappproject.utils.GenreTypes
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter

class MainPageGameAdapter(

    onItemBind: (GenreTypes) -> Unit,
    onReadyToLoadMore: (GenreTypes, Int) -> Unit,
    onGameClick: (GameUi) -> Unit,
    scrollStates: MutableMap<Int, Parcelable?>
) :
    AsyncListDifferDelegationAdapter<GameListUi>(DIFF_CALLBACK) {
    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<GameListUi> =
            object : DiffUtil.ItemCallback<GameListUi>() {
                override fun areItemsTheSame(oldItem: GameListUi, newItem: GameListUi): Boolean =
                    oldItem == newItem

                override fun areContentsTheSame(oldItem: GameListUi, newItem: GameListUi): Boolean {
                    return oldItem == newItem
                }
            }
    }

    init {
        delegatesManager.addDelegate(
            MainPageDelegate.gamesHorizontalDelegate(
                onItemBind = onItemBind,
                onReadyToLoadMore = onReadyToLoadMore,
                onGameClick = onGameClick,
                scrollStates = scrollStates
            )
        )
    }
}
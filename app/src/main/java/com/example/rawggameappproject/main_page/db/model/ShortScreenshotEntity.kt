package com.example.rawggameappproject.main_page.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "short_screenshots",
    indices = [
        Index("id")
    ]
)

data class ShortScreenshotEntity(
    @ColumnInfo(name = "game_id")
    val gameId: Int,
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "image")
    val image: String
)

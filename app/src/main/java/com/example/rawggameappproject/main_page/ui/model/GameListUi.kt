package com.example.rawggameappproject.main_page.ui.model

import android.os.Parcelable
import com.example.rawggameappproject.utils.GenreTypes
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize

@Parcelize
data class GameListUi(
    val genre: GenreTypes,
    val results: List<GameUi>
): Parcelable



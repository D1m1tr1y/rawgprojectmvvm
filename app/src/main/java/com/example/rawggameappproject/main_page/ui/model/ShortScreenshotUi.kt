package com.example.rawggameappproject.main_page.ui.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ShortScreenshotUi(
    val id: Int,
    val image: String
):Parcelable

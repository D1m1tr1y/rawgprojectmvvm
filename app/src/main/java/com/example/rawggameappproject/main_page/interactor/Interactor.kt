package com.example.rawggameappproject.main_page.interactor

import com.example.rawggameappproject.main_page.ui.model.GameListUi
import com.example.rawggameappproject.utils.GenreTypes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

interface Interactor {
    fun data(): Flow<List<GameListUi>>
    suspend fun initCategory(genre: GenreTypes, scope: CoroutineScope)
    suspend fun tryToLoadMore(genre: GenreTypes, index: Int, scope: CoroutineScope)
}
package com.example.rawggameappproject.main_page.model
data class Genre(
    val id: Int,
    val name: String,
    val slug: String
)

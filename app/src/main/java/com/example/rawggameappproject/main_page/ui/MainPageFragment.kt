package com.example.rawggameappproject.main_page.ui

import android.os.Parcelable
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rawggameappproject.R
import com.example.rawggameappproject.common.BaseFragment
import com.example.rawggameappproject.databinding.FragmentMainPageBinding
import com.example.rawggameappproject.details.DetailsPageFragment
import com.example.rawggameappproject.main_page.ui.main_adapter.MainPageGameAdapter
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.example.rawggameappproject.utils.ext.addScreen
import com.example.rawggameappproject.utils.ext.replaceScreen
import com.example.rawggameappproject.utils.viewBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainPageFragment : BaseFragment(R.layout.fragment_main_page) {
    private val binding: FragmentMainPageBinding by viewBinding()
    private val viewModel: MainPageViewModel by viewModels()
    private val scrollStates: MutableMap<Int, Parcelable?> = mutableMapOf()

    private val adapter by lazy {
        MainPageGameAdapter(
            onItemBind = viewModel::initCategory,
            onReadyToLoadMore = viewModel::readyToLoadMore,
            onGameClick = ::onGameClick,
            scrollStates = scrollStates
        )
    }

    override fun bind() {
        with(viewModel) {
            observe(gameData) { adapter.items = it }
        }
    }

    override fun initViews(view: View) {
        with(binding) {
            genreRecyclerView.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            genreRecyclerView.adapter = adapter
            genreRecyclerView.setHasFixedSize(true)
            genreRecyclerView.itemAnimator = null
            adapter.onAttachedToRecyclerView(genreRecyclerView)
        }
    }

    private fun onGameClick(game: GameUi) {
        replaceScreen(DetailsPageFragment.newInstance(game))
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.finish()
    }


}
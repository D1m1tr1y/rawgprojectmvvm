package com.example.rawggameappproject.main_page.model

import com.example.rawggameappproject.main_page.api.model.GameListResponse
import com.example.rawggameappproject.main_page.api.model.GenreResponse
import com.example.rawggameappproject.main_page.api.model.ParentPlatformResponse
import com.example.rawggameappproject.main_page.api.model.RatingResponse
import project.rawg.mainpage.api.model.ShortScreenshotResponse

object ResponseGameDataConverter {
    fun fromNetwork(response : GameListResponse): List<Game> =
        response.results.map { result ->
            Game(
                id = result.id,
                slug = result.slug,
                name = result.name,
                released = result.released,
                backgroundImage = result.backgroundImage,
                rating = result.rating,
                ratingsCount = result.ratingsCount,
                ratings = fromNetworkRatings(result.ratings),
                added = result.added,
                metacritic = result.metacritic,
                parentPlatforms = fromNetworkParentPlatforms(result.parentPlatforms),
                genres = fromNetworkGenre(result.genres),
                esrbRatingSlug = result.esrbRating?.slug,
                esrbRatingName = result.esrbRating?.name,
                esrbRatingId = result.esrbRating?.id,
                shortScreenshots = fromNetworkShortScreenshots(result.shortScreenshots)
            )
        }
    private fun fromNetworkRatings(response : List<RatingResponse>) : List<Rating> =
        response.map {result->
            Rating(
                id = result.id,
                title = result.title,
                count = result.count,
                percent = result.percent
            )
        }
    private fun fromNetworkParentPlatforms(response : List<ParentPlatformResponse>) : List<ParentPlatform> =
        response.map { result->
            ParentPlatform(
                id = result.parentPlatformInfo.id,
                name = result.parentPlatformInfo.name,
                slug = result.parentPlatformInfo.slug
            )
        }
    private fun fromNetworkGenre(response: List<GenreResponse>): List<Genre> =
        response.map { result ->
            Genre(
                id = result.id,
                name = result.name,
                slug = result.slug
            )
        }
    private fun fromNetworkShortScreenshots(response: List<ShortScreenshotResponse>):List<ShortScreenshot> =
        response.map { result->
            ShortScreenshot(
                id = result.id,
                image = result.image
            )
        }


}
package com.example.rawggameappproject.main_page.model

import com.example.rawggameappproject.main_page.api.PagingState
import com.example.rawggameappproject.utils.GenreTypes

data class GameCategoryModel(
    val genreType: GenreTypes,
    val dataState: PagingState<List<Game>>
)

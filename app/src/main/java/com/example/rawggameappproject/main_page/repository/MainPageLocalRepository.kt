package com.example.rawggameappproject.main_page.repository

import com.example.rawggameappproject.main_page.db.dao.MaInPageDao
import com.example.rawggameappproject.main_page.db.model.EntityGamesDataConverter
import com.example.rawggameappproject.main_page.model.Game
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MainPageLocalRepository @Inject constructor(
    private val dao: MaInPageDao
) : LocalRepository {
    override suspend fun upsertGamesData(gamesData: List<Game>,genre: String) {
        val response = EntityGamesDataConverter.toDataBase(gamesData,genre)
        dao.upsertGames(response)
        gamesData.forEach { result ->
            dao.upsertGenres(
                EntityGamesDataConverter.toDataBaseGenre(result.genres, result.id)
            )
            dao.upsertRatings(
                EntityGamesDataConverter.toDataBaseRating(result.ratings, result.id)
            )
            dao.upsertParentPlatforms(
                EntityGamesDataConverter.toDataBaseParentPlatform(result.parentPlatforms, result.id)
            )
            dao.upsertShortScreenshots(
                EntityGamesDataConverter.toDataBaseShortScreenshot(
                    result.shortScreenshots,
                    result.id
                )
            )
        }

    }

    override suspend fun getGamesData(genre: String): Flow<List<Game>> =
        dao.getGames(genre).map { result ->
            result.map{game ->
                 EntityGamesDataConverter.fromDataBase(
                    response = game,
                    parentPlatform = dao.getParentPlatforms(game.gameId),
                    rating = dao.getRating(game.gameId),
                    genre = dao.getGenres(game.gameId),
                    shortScreenshot = dao.getShortScreenshots(game.gameId)
                )
            }
    }
}
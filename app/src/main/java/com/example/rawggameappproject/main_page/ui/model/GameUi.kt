package com.example.rawggameappproject.main_page.ui.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GameUi(
    val id: Int,
    val slug: String,
    val name: String,
    val released: String,
    val backgroundImage: String,
    val rating: Float,
    val ratings: List<RatingUi>,
    val ratingsCount: Int,
    val added: Int,
    val metacritic: Int?,
    val parentPlatforms: List<ParentPlatformUi>,
    val genres: List<GenreUi>,
    val esrbRatingId: Int?,
    val esrbRatingName: String?,
    val esrbRatingSlug: String?,
    val shortScreenshots: List<ShortScreenshotUi>,
) : Parcelable

package com.example.rawggameappproject.main_page.ui.main_adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter

class MainPageGenreAdapter(
        onReadyToLoadMore: (Int) -> Unit,
onGameClick: (GameUi) -> Unit,
) :
AsyncListDifferDelegationAdapter<GameUi>(DIFF_CALLBACK) {


    companion object {
        val DIFF_CALLBACK: DiffUtil.ItemCallback<GameUi> =
            object : DiffUtil.ItemCallback<GameUi>() {
                override fun areItemsTheSame(oldItem: GameUi, newItem: GameUi): Boolean =
                    oldItem == newItem

                override fun areContentsTheSame(oldItem: GameUi, newItem: GameUi): Boolean {
                    return oldItem == newItem
                }
            }
    }

    init {
        delegatesManager.addDelegate(
            MainPageDelegate.gameDelegate(
                onReadyToLoadMore,
                onGameClick
            )
        )
    }

}

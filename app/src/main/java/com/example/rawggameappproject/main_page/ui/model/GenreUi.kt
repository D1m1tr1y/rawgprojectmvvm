package com.example.rawggameappproject.main_page.ui.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class GenreUi(
    val id: Int,
    val name: String,
    val slug: String
):Parcelable

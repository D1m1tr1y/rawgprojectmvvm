package com.example.rawggameappproject.main_page.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "games",
    indices = [Index(value = ["id", "genre_type"], unique = true)]
)

data class GameEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "game_id")
    var gameId: Long = 0,
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "slug")
    val slug: String,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "genre_type")
    val genreType: String,
    @ColumnInfo(name = "released")
    val released: String,
    @ColumnInfo(name = "background_image")
    val backgroundImage: String,
    @ColumnInfo(name = "rating")
    val rating: Float,
    @ColumnInfo(name = "ratings_count")
    val ratingsCount: Int,
    @ColumnInfo(name = "added")
    val added: Int,
    @ColumnInfo(name = "metacritic")
    val metacritic: Int?,
    @ColumnInfo(name = "esrb_rating_id")
    val esrbRatingId: Int?,
    @ColumnInfo(name = "esrb_rating_name")
    val esrbRatingName: String?,
    @ColumnInfo(name = "esrb_rating_slug")
    val esrbRatingSlug: String?
    )


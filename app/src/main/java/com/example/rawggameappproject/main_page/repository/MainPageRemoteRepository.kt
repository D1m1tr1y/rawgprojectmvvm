package com.example.rawggameappproject.main_page.repository

import com.example.rawggameappproject.main_page.api.MainPageApi
import com.example.rawggameappproject.main_page.model.Game
import com.example.rawggameappproject.main_page.model.ResponseGameDataConverter
import com.example.rawggameappproject.utils.Constants
import com.example.rawggameappproject.utils.Constants.DEFAULT_PAGE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainPageRemoteRepository @Inject constructor(
  val api : MainPageApi
) : RemoteRepository{
    override suspend fun getGamesData(genre : String,page : Int): List<Game> {
    return    ResponseGameDataConverter.fromNetwork(
            api.getGamesData(
                genre = genre,
                page = page
            )
        )
    }
    private var genre: String? = ""
    private var page = DEFAULT_PAGE

    override fun updateParams(genre: String, alreadyLoadedCount: Int) {
        this.genre = genre
        if (alreadyLoadedCount < 0) throw IllegalArgumentException()
        page = (alreadyLoadedCount / (Constants.PAGE_SIZE + 1)) + 1
    }

    override suspend fun initialLoading(genre: String): List<Game> {
        return withContext(Dispatchers.IO) {
            val response = api.getGamesData(genre = genre)
            this@MainPageRemoteRepository.genre = genre
            ResponseGameDataConverter.fromNetwork(response)
        }
    }

    override suspend fun loadMore(): List<Game> {
        return withContext(Dispatchers.IO) {
            val genre = this@MainPageRemoteRepository.genre ?: throw IllegalStateException()
            val response = api.getGamesData(genre = genre, page = page + 1)
            page += 1
            ResponseGameDataConverter.fromNetwork(response)
        }
    }


}

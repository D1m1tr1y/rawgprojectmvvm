package com.example.rawggameappproject.main_page.ui.main_adapter

import android.os.Parcelable
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import coil.transform.RoundedCornersTransformation
import com.example.rawggameappproject.databinding.ItemGenresRecuclerviewBinding
import com.example.rawggameappproject.databinding.ItemMainRecyclerviewBinding
import com.example.rawggameappproject.main_page.ui.model.GameListUi
import com.example.rawggameappproject.main_page.ui.model.GameUi
import com.example.rawggameappproject.utils.GenreTypes
import com.example.rawggameappproject.utils.ext.onClick
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

object MainPageDelegate {
    private const val IMAGE_LOADING_DURATION = 600
    private const val ROUNDED_CORNERS_RADIUS = 16f

    fun gameDelegate(
        onReadyToLoadMore: (Int) -> Unit,
        onGameClick: (GameUi) -> Unit
    ) = adapterDelegateViewBinding<GameUi, GameUi, ItemMainRecyclerviewBinding>(
        { inflater, container -> ItemMainRecyclerviewBinding.inflate(inflater, container, false) }
    ) {
        bind {
            with(binding) {
                gameName.text = item.name
                image.load(item.backgroundImage) {
                    transformations(RoundedCornersTransformation(25f))
                }
                onReadyToLoadMore.invoke(bindingAdapterPosition)
                binding.root.onClick { onGameClick.invoke(item) }
            }
        }
    }


    fun gamesHorizontalDelegate(
        onItemBind: (GenreTypes) -> Unit,
        onReadyToLoadMore: (GenreTypes, Int) -> Unit,
        onGameClick: (GameUi) -> Unit,
        scrollStates: MutableMap<Int, Parcelable?>
    ) =
        adapterDelegateViewBinding<GameListUi, GameListUi, ItemGenresRecuclerviewBinding>(
            { inflater, container ->
                ItemGenresRecuclerviewBinding.inflate(inflater, container, false)
            }
        ) {
            val adapter = MainPageGenreAdapter(
                onReadyToLoadMore = { pos -> onReadyToLoadMore.invoke(item.genre, pos) },
                onGameClick = onGameClick,
            )
            with(binding) {
                genreRecyclerView.layoutManager =
                    LinearLayoutManager(binding.root.context, LinearLayoutManager.HORIZONTAL, false)
                genreRecyclerView.adapter = adapter
                bind {
                    onItemBind.invoke(item.genre)
                    adapter.apply {
                        genreName.text = item.genre.Genre
                        items = item.results
                    }
                    scrollStates[bindingAdapterPosition]?.let {
                        genreRecyclerView.layoutManager?.onRestoreInstanceState(it)
                        scrollStates.remove(bindingAdapterPosition)
                    }
                }
                onViewRecycled {
                    genreRecyclerView.layoutManager?.onSaveInstanceState()?.let {
                        scrollStates[bindingAdapterPosition] = it
                    }
                }
            }
        }
}

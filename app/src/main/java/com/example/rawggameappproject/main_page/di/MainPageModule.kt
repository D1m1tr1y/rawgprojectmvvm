package com.example.rawggameappproject.main_page.di

import android.content.Context
import androidx.room.Room
import com.example.rawggameappproject.db.GamesMediator
import com.example.rawggameappproject.db.RoomDataBase
import com.example.rawggameappproject.main_page.api.MainPageApi
import com.example.rawggameappproject.main_page.db.dao.MaInPageDao
import com.example.rawggameappproject.main_page.interactor.Interactor
import com.example.rawggameappproject.main_page.repository.LocalRepository
import com.example.rawggameappproject.main_page.repository.MainPageLocalRepository
import com.example.rawggameappproject.main_page.repository.MainPageRemoteRepository
import com.example.rawggameappproject.main_page.repository.RemoteRepository
import com.example.rawggameappproject.utils.GenreTypes
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import com.example.rawggameappproject.main_page.interactor.MainPageInteractor
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Qualifier
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class MainPageModule {
    @Binds
    @Singleton
    abstract fun bindRemoteRepository(repository: MainPageRemoteRepository): RemoteRepository

    @Binds
    @Singleton
    abstract fun bindLocalRepository(repository: MainPageLocalRepository): LocalRepository
    @Binds
    @Singleton
    abstract fun bindInteractor(interactor: MainPageInteractor): Interactor


    companion object {
        @IoDispatcher
        @Provides
        fun provideContextIo(): CoroutineDispatcher = Dispatchers.IO

        @DefaultDispatcher
        @Provides
        fun provideContextDefault(): CoroutineDispatcher = Dispatchers.Default

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class IoDispatcher

        @Retention(AnnotationRetention.RUNTIME)
        @Qualifier
        annotation class DefaultDispatcher

        @Provides
        @Singleton
        fun provideApi(retrofit: Retrofit): MainPageApi =
            retrofit.create(MainPageApi::class.java)



        @Provides
        @Singleton
        fun provideChannelDao(appDatabase: RoomDataBase): MaInPageDao =
            appDatabase.gameDao

        @Provides
        @Singleton
        fun provideAppDatabase(@ApplicationContext appContext: Context): RoomDataBase =
            Room.databaseBuilder(
                appContext,
                RoomDataBase::class.java,
                "Database"
            ).build()
        @Provides
        @Singleton
        @Named("action")
        fun actionMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Action,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("indie")
        fun indieMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Indie,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("adventure")
        fun adventureMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Adventure,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("rpg")
        fun rpgMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Rpg,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("strategy")
        fun strategyMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Strategy,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("shooter")
        fun shooterMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Shooter,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("casual")
        fun casualMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Casual,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("simulation")
        fun simulationMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Simulation,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("puzzle")
        fun puzzleMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Puzzle,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("arcade")
        fun arcadeMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Arcade,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("platformer")
        fun platformerMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Platformer,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("massive")
        fun massiveMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.MassivelyMultiplayer,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("racing")
        fun racingMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Racing,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("sports")
        fun sportsMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Sports,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("fighting")
        fun fightingMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Fighting,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("family")
        fun familyMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Family,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("board")
        fun boardMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.BoardGames,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("edu")
        fun eduMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Educational,
            defaultDispatcher
        )

        @Provides
        @Singleton
        @Named("card")
        fun cardMediator(
            remoteRepository: MainPageRemoteRepository,
            localRepository: MainPageLocalRepository,
            @DefaultDispatcher defaultDispatcher: CoroutineDispatcher
        ): GamesMediator = GamesMediator(
            remoteRepository,
            localRepository,
            GenreTypes.Card,
            defaultDispatcher
        )
    }

}


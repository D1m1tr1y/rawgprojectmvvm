package com.example.rawggameappproject.main_page.ui.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RatingUi(
    val id: Int,
    val title: String,
    val count: Int,
    val percent: Float
):Parcelable

package com.example.rawggameappproject.main_page.db.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "parent_platforms",
    indices = [Index(value = ["id", "game_id"], unique = true)]
)

data class ParentPlatformEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "platform_id")
    val platformId: Long = 0,
    @ColumnInfo(name = "game_id")
    val gameId: Int,
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "slug")
    val slug: String
)

package com.example.rawggameappproject.main_page.api
import com.example.rawggameappproject.main_page.api.model.GameListResponse
import com.example.rawggameappproject.utils.Constants.DEFAULT_PAGE
import com.example.rawggameappproject.utils.Constants.KEY
import com.example.rawggameappproject.utils.Constants.PAGE_SIZE
import retrofit2.http.GET
import retrofit2.http.Query


interface MainPageApi {
    @GET("api/games")
    suspend fun getGamesData(
        @Query("key") key: String = KEY,
        @Query("genres") genre: String,
        @Query("page_size") pageSize: Int = PAGE_SIZE,
        @Query("page") page: Int = DEFAULT_PAGE
    ): GameListResponse
}

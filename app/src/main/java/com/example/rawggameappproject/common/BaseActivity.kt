package com.example.rawggameappproject.common

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

abstract class BaseActivity : AppCompatActivity() {
     fun replace(fragment: Fragment, id: Int) {
        this.supportFragmentManager
            .beginTransaction()
            .addToBackStack(null)
            .replace(id, fragment)
            .commit()
    }
}
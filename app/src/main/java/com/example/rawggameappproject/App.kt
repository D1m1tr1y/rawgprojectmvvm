package com.example.rawggameappproject

import android.app.Application
import com.example.rawggameappproject.common.di.NetworkModule
import com.example.rawggameappproject.main_page.di.MainPageModule
import dagger.hilt.android.HiltAndroidApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import timber.log.Timber
@HiltAndroidApp
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        setTimber()
    }

    private fun setTimber() {
        Timber.plant(Timber.DebugTree())
    }
}

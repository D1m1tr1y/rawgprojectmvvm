package com.example.rawggameappproject.root

import android.os.Bundle
import com.example.rawggameappproject.R
import com.example.rawggameappproject.common.BaseActivity
import com.example.rawggameappproject.main_page.ui.MainPageFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RootActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
       replace(MainPageFragment(),R.id.container)
    }
}
